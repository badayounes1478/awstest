const mongoose = require('mongoose')
const schema = mongoose.Schema

const user = new schema({
    name: String,
    sirName: String
})

module.exports = mongoose.model("user", user)